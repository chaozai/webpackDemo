学习了webpack门级的教程后，觉得可能是专门为单页应用而量身打造的，比如webpack+react、webpack+vue等，都可以解决各种资源的依赖加载、打包的问题。甚至css都是打包在js里去动态添加到dom文档中的。

> 那如果我们想要想要多页的普通的web站点，css独立出来，js加载需要模块？

项目地址：[http://git.oschina.net/zycBlogs/webpackDemo](http://git.oschina.net/zycBlogs/webpackDemo)

#### 初始化项目、安装依赖

package.json

``` javascript 
"devDependencies": {
    "css-loader": "^0.23.1",
    "extract-text-webpack-plugin": "^1.0.1",
    "file-loader": "^0.8.5",
    "html-loader": "^0.4.3",
    "html-webpack-plugin": "^2.9.0",
    "jquery": "^1.12.0",
    "less": "^2.6.0",
    "less-loader": "^2.2.2",
    "sass-loader": "^4.0.2",
    "style-loader": "^0.13.0",
    "url-loader": "^0.5.7",
    "webpack": "^1.12.13",
    "webpack-dev-server": "^1.14.1"
}
```
#### 目录结构（我用的express框架，其他的根据个人需求）

```
- webpackDemo
    - src                #代码开发目录
        - css            #css目录，按照页面（模块）、通用、第三方三个级别进行组织
            + page
            + common
            + lib
        - js             #JS脚本，按照page、components进行组织
            + page
            + components
        + template           #HTML模板
    - node_modules       #所使用的nodejs模块
    - public                        #express静态资源文件
        - dist                       #webpack编译打包输出目录，无需建立目录可由webpack根据配置自动生成
            + css                
            + js
        + img            #图片资源
    + view                       #express静态资源文件（webpack编译打包输出view目录）
    package.json            #项目配置
    webpack.config.js    #webpack配置
```

#### 开发页面

在src/js/page目录下建立index.js文件，在src/view目录下建立index.html文件。入口js和模板文件名对应。

index.html 内容如下：

``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
    <!--
        描述：head中无需再引入css以及facicon，webpack将根据入口JS文件的要求自动实现按需加载或者生成style标签
    -->
</head>
<body>
    <!--
        描述：body中同样无需单独引入JS文件，webpack会根据入口JS文件自动实现按需加载或者生成script标签，还可以生成对应的hash值
    -->
</body>
</html>
```

就是这样一个简单的HTML模板，不要引入任何CSS和JS，通过webpack打包就可以自动帮我们引入。


index.js内容如下：

```javascript
//引入css
require("../../css/lib/base.css");
require("../../css/page/index.scss");


$('body').append('<p class="text">index</p>');
```

page1.html:

```xml
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>page1</title>
</head>
<body>

</body>
</html>
```

page1.js:

```javascript
//引入css
require("../../css/lib/base.css");
require("../../css/page/page1.less");

$('body').html('page1');
```


#### webpack配置（我用的express框架，其他的根据个人需求）

```javascript
var path = require('path');
var webpack = require('webpack');
/*
extract-text-webpack-plugin插件，
有了它就可以将你的样式提取到单独的css文件里，
妈妈再也不用担心样式会被打包到js文件里了。
 */
var ExtractTextPlugin = require('extract-text-webpack-plugin');
/*
html-webpack-plugin插件，重中之重，webpack中生成HTML的插件，
具体可以去这里查看https://www.npmjs.com/package/html-webpack-plugin
 */
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: { //配置入口文件，有几个写几个
        index: './src/js/page/index.js',
        page1: './src/js/page/page1.js'
    },
    output: { 
        path: path.join(__dirname, './public/dist/'), //输出目录的配置，模板、样式、脚本、图片等资源的路径配置都相对于它
        publicPath: '/dist/',               //模板、样式、脚本、图片等资源对应的server上的路径
        filename: 'js/[name].js',           //每个页面对应的主js的生成配置
        chunkFilename: 'js/[id].chunk.js'   //chunk生成的配置
    },
    module: {
        loaders: [ //加载器，关于各个加载器的参数配置，可自行搜索之。
            {
                test: /\.css$/,
                //配置css的抽取器、加载器。'-loader'可以省去
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader') 
            }, {
                test: /\.less$/,
                //配置less的抽取器、加载器。中间!有必要解释一下，
                //根据从右到左的顺序依次调用less、css加载器，前一个的输出是后一个的输入
                //你也可以开发自己的loader哟。有关loader的写法可自行谷歌之。
                loader: ExtractTextPlugin.extract('css!less')
            }, {
                test: /\.scss$/,
                //配置scss的抽取器、加载器。中间!有必要解释一下，
                //根据从右到左的顺序依次调用scss、css加载器，前一个的输出是后一个的输入
                //你也可以开发自己的loader哟。有关loader的写法可自行谷歌之。
                loader: ExtractTextPlugin.extract('css!scss')
            }, {
                //html模板加载器，可以处理引用的静态资源，默认配置参数attrs=img:src，处理图片的src引用的资源
                //比如你配置，attrs=img:src img:data-src就可以一并处理data-src引用的资源了，就像下面这样
                test: /\.html$/,
                loader: "html?attrs=img:src img:data-src"
            }, {
                //文件加载器，处理文件静态资源
                test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=./fonts/[name].[ext]'
            }, {
                //图片加载器，雷同file-loader，更适合图片，可以将较小的图片转成base64，减少http请求
                //如下配置，将小于8192byte的图片转成base64码
                test: /\.(png|jpg|gif)$/,
                loader: 'url-loader?limit=8192&name=./img/[hash].[ext]'
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({ //加载jq
            $: 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons', // 将公共模块提取，生成名为`commons`的chunk
            chunks: ['index','page1'], //提取哪些模块共有的部分
            minChunks: 2 // 提取至少2个模块共有的部分
        }),
        new ExtractTextPlugin('css/[name].css'), //单独使用link标签加载css并设置路径，相对于output配置中的publickPath
        
        //HtmlWebpackPlugin，模板生成相关的配置，每个对于一个页面的配置，有几个写几个
        new HtmlWebpackPlugin({ //根据模板插入css/js等生成最终HTML
            favicon: './src/favicon.ico', //favicon路径，通过webpack引入同时可以生成hash值
            filename: '../../views/index.html', //生成的html存放路径，相对于path
            template: './src/template/index.html', //html模板路径
            inject: 'body', //js插入的位置，true/'head'/'body'/false
            hash: true, //为静态资源生成hash值
            chunks: ['commons', 'index'],//需要引入的chunk，不配置就会引入所有页面的资源
            minify: { //压缩HTML文件    
                removeComments: true, //移除HTML中的注释
                collapseWhitespace: false //删除空白符与换行符
            }
        }),
        new HtmlWebpackPlugin({ //根据模板插入css/js等生成最终HTML
            favicon: './src/favicon.ico', //favicon路径，通过webpack引入同时可以生成hash值
            filename: '../../views/page1.html', //生成的html存放路径，相对于path
            template: './src/template/page1.html', //html模板路径
            inject: true, //js插入的位置，true/'head'/'body'/false
            hash: true, //为静态资源生成hash值
            chunks: ['commons', 'list'],//需要引入的chunk，不配置就会引入所有页面的资源
            minify: { //压缩HTML文件    
                removeComments: true, //移除HTML中的注释
                collapseWhitespace: false //删除空白符与换行符
            }
        })

        // new webpack.HotModuleReplacementPlugin() //热加载
    ],
    //使用webpack-dev-server，提高开发效率
    // devServer: {
    //     contentBase: './',
    //     host: 'localhost',
    //     port: 9090, //默认8080
    //     inline: true, //可以监控js变化
    //     hot: true, //热启动
    // }
};
```
好了，完成以上的这些配置之后，执行`webpack`打包命令完成项目打包。

```sell
Hash: e6219853995506fd132a
Version: webpack 1.14.0
Time: 1338ms
                Asset       Size  Chunks             Chunk Names
          js/index.js  457 bytes       0  [emitted]  index
          js/page1.js  392 bytes       1  [emitted]  page1
        js/commons.js     306 kB       2  [emitted]  commons
        css/index.css   62 bytes       0  [emitted]  index
        css/page1.css   62 bytes       1  [emitted]  page1
      css/commons.css  803 bytes       2  [emitted]  commons
          favicon.ico    1.15 kB          [emitted]
../../view/index.html  496 bytes          [emitted]
../../view/page1.html  499 bytes          [emitted]
   [0] ./src/js/page/index.js 170 bytes {0} [built]
   [0] ./src/js/page/page1.js 106 bytes {1} [built]
    + 7 hidden modules
Child html-webpack-plugin for "../../view/page1.html":
        + 1 hidden modules
Child html-webpack-plugin for "../../view/index.html":
        + 1 hidden modules
Child extract-text-webpack-plugin:
        + 2 hidden modules
Child extract-text-webpack-plugin:
        + 2 hidden modules
Child extract-text-webpack-plugin:
        + 2 hidden modules
```


此时，前往views目录下查看生成的index.html文件，如下：

```xml
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
    
<link rel="shortcut icon" href="/dist/favicon.ico"><link href="/dist/css/commons.css?e6219853995506fd132a" rel="stylesheet"><link href="/dist/css/index.css?e6219853995506fd132a" rel="stylesheet"></head>
<body>
    
<script type="text/javascript" src="/dist/js/commons.js?e6219853995506fd132a"></script><script type="text/javascript" src="/dist/js/index.js?e6219853995506fd132a"></script></body>
</html>
```

可以看到生成的文件除了保留原模板中的内容以外，还根据入口文件index.js的定义，自动添加需要引入CSS与JS文件，以及favicon，同时还添加了相应的hash值。


> ## 两个问题

* webpack如何自动发现entry文件及进行相应的模板配置
* 如何直接处理样式、脚本自动引入问题

```javascript
var path = require('path');
var webpack = require('webpack');
var glob = require('glob');
/*
extract-text-webpack-plugin插件，
有了它就可以将你的样式提取到单独的css文件里，
妈妈再也不用担心样式会被打包到js文件里了。
 */
var ExtractTextPlugin = require('extract-text-webpack-plugin');
/*
html-webpack-plugin插件，重中之重，webpack中生成HTML的插件，
具体可以去这里查看https://www.npmjs.com/package/html-webpack-plugin
 */
var HtmlWebpackPlugin = require('html-webpack-plugin');
/**
 *将公共模块提取，生成名为`commons`的chunk
 */
var CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
//压缩
var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;

//判断开发模式
var debug = process.env.NODE_ENV !== 'production';

var getEntry = function(globPath, pathDir) {
    var files = glob.sync(globPath);
    var entries = {},
        entry, dirname, basename, pathname, extname;

    for (var i = 0; i < files.length; i++) {
        entry = files[i];
        dirname = path.dirname(entry);   //文件目录
        extname = path.extname(entry);   //后缀名
        basename = path.basename(entry, extname);  //文件名
        pathname = path.join(dirname, basename);
        pathname = pathDir ? pathname.replace(new RegExp('^' + pathDir), '') : pathname;
        entries[pathname] = ['./' + entry];  //这是在osx系统下这第写  win7  entries[basename]
    }
    console.log(entries);
    return entries;
}

//入口(通过getEntry方法得到所有的页面入口文件)
var entries = getEntry('src/js/page/**/*.js', 'src/js/page/');
//提取哪些模块共有的部分从entries里面获得文件名称
var chunks = Object.keys(entries);
//模板页面(通过getEntry方法得到所有的模板页面)
var pages = Object.keys(getEntry('src/template/**/*.html', 'src/template/'));

console.log(pages)




var config = {
    entry: entries,
    output: {
        path: path.join(__dirname, './public/dist/'),//输出目录的配置，模板、样式、脚本、图片等资源的路径配置都相对于它
        publicPath: '/dist/',               //模板、样式、脚本、图片等资源对应的server上的路径
        filename: 'js/[name].js',           //每个页面对应的主js的生成配置
        chunkFilename: 'js/[id].chunk.js?[chunkhash]'   //chunk生成的配置
    },
    module: {
        loaders: [ //加载器
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css')
            }, {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('css!less')
            }, {
                test: /\.html$/,
                loader: "html?-minimize"    //避免压缩html,https://github.com/webpack/html-loader/issues/50
            }, {
                test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            }, {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'url-loader?limit=8192&name=imgs/[name]-[hash].[ext]'
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({ //加载jq
            $: 'jquery'
        }),
        new CommonsChunkPlugin({
            name: 'commons', // 将公共模块提取，生成名为`commons`的chunk
            chunks: chunks,
            minChunks: chunks.length // 提取所有entry共同依赖的模块
        }),
        new ExtractTextPlugin('css/[name].css'), //单独使用link标签加载css并设置路径，相对于output配置中的publickPath
        debug ? function() {} : new UglifyJsPlugin({ //压缩代码
            compress: {
                warnings: false
            },
            except: ['$super', '$', 'exports', 'require'] //排除关键字
        }),
    ]
};

pages.forEach(function(pathname) {
    var conf = {
        filename: '../../views/' + pathname + '.html', //生成的html存放路径，相对于path
        template: 'src/template/' + pathname + '.html', //html模板路径
        inject: false,  //js插入的位置，true/'head'/'body'/false
        /*
        * 压缩这块，调用了html-minify，会导致压缩时候的很多html语法检查问题，
        * 如在html标签属性上使用{{...}}表达式，所以很多情况下并不需要在此配置压缩项，
        * 另外，UglifyJsPlugin会在压缩代码的时候连同html一起压缩。
        * 为避免压缩html，需要在html-loader上配置'html?-minimize'，见loaders中html-loader的配置。
         */
        // minify: { //压缩HTML文件
        //  removeComments: true, //移除HTML中的注释
        //  collapseWhitespace: false //删除空白符与换行符
        // }
    };
    if (pathname in config.entry) {
        favicon: './src/favicon.ico', //favicon路径，通过webpack引入同时可以生成hash值
        conf.inject = 'body';
        conf.chunks = ['commons', pathname];
        conf.hash = true;
    }
    config.plugins.push(new HtmlWebpackPlugin(conf));
});

module.exports = config;    
```

下面的代码和上面的差不多，本质上的区别就是把通过一个方法把所有的相关的文件放到一个对象里这样就完成了自动引入的效果了！

以上均为在mac osx 系统的配置，win7路径可能会有不同


glob: 这边解析出来的不一样：
但要求最终
entries:
{ 
    index: [ './src/template/index.js' ],
    page1: [ './src/template/page1.js' ] 
}

pages:
[ 'index', 'page1' ]

要以根据个人电脑的配置相应的更改